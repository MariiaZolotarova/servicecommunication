﻿using RabbitMQ.Client.Events;
using RabbitMQWrapper;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    public class Program
    {
        public static void Main()
        {
            const string sendQueue = "ping_queue";
            const string listenQueue = "pong_queue";

            var rabbitMQ = new RabbitMQService();
            EventHandler<BasicDeliverEventArgs> eventHandler = (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message + " " + DateTime.UtcNow);
                Thread.Sleep(2500);
                rabbitMQ.SendMessageToQueue(sendQueue, "pong");
            };

            rabbitMQ.QueueDeclare(listenQueue);
            rabbitMQ.QueueDeclare(sendQueue);
            rabbitMQ.ListenQueue(listenQueue, eventHandler);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
            rabbitMQ.Dispose();
        }
    }
}