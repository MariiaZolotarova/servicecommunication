﻿using System;
using System.Text;
using System.Threading.Channels;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQWrapper
{
    public class RabbitMQService : IDisposable
    {
        private const string hostName = "localhost";
        private readonly IConnection connection;
        private readonly IModel channel;

        public RabbitMQService()
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
        }

        public void QueueDeclare(string queue)
        {
            channel.QueueDeclare(queue: queue,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
        }

        public void SendMessageToQueue(string toQueue, string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;

            channel.BasicPublish(exchange: "",
                                 routingKey: toQueue,
                                 basicProperties: properties,
                                 body: body);
        }

        public void ListenQueue(string queueName, EventHandler<BasicDeliverEventArgs> eventHandler)
        {
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += eventHandler;
            channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }

        public void Dispose()
        {
            connection.Close();
            channel.Close();
        }
    }
}